import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PortfolioMetroComponent } from "./layouts/portfolio-metro/portfolio-metro.component";
import { AboutComponent } from "./layouts/about/about.component";
import { ContactComponent } from "./layouts/contact/contact.component";

export const routes: Routes = [
  {
    path: "",
    redirectTo: "portfolio-layout",
    pathMatch: "full",
  },
  {
    path: "portfolio-layout",
    component: PortfolioMetroComponent,
  },
  {
    path: "galerie",
    component: AboutComponent,
  },
  {
    path: "aboutme",
    component: AboutComponent,
  },
  {
    path: "contact",
    component: ContactComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // preloadingStrategy: PreloadAllModules,
      anchorScrolling: "enabled",
      scrollPositionRestoration: "enabled",
      initialNavigation: "enabled",
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
