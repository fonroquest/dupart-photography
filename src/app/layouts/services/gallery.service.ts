import { Injectable } from "@angular/core";
import { Image } from "@ks89/angular-modal-gallery";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

import { data } from "../portfolio-metro/gallery-data";
@Injectable({
  providedIn: "root",
})
export class GalleryService {
  constructor(private http: HttpClient) {}

  getImages(): Observable<any[]> {
    return this.http.get<any[]>("/api/gallery");
  }

  getImagesViaTags(tag: string[]): Image[] {
    if (tag.length === 0)
      return data.map((i: any) => {
        return new Image(i.id, { img: i.url });
      });

    const tmp: any = data.filter((i) => {
      let toto = true;
      tag.forEach((t) => (toto = toto && i.tags.includes(t)));
      return toto;
    });

    return tmp.map((i: any) => {
      return new Image(i.id, { img: i.url });
    });
  }
}
