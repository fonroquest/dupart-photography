import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-instagram-integration",
  templateUrl: "./instagram-integration.component.html",
  styleUrls: ["./instagram-integration.component.scss"],
})
export class InstagramIntegrationComponent implements OnInit {
  gallery = [
    {
      img: "https://picsum.photos/400/300",
    },
    {
      img: "https://picsum.photos/300/400",
    },
    {
      img: "https://picsum.photos/500/450",
    },
    {
      img: "https://picsum.photos/400",
    },
    {
      img: "https://picsum.photos/400",
    },
    {
      img: "https://picsum.photos/400",
    },
  ];

  galleryCarouselOptions = {
    items: 3,
    autoHeight: true,
    nav: true,
    navText: [
      '<a href="javascript:void(0)" class="btn btn-default primary-btn light"><i class="fa fa-arrow-left"></i></a>',
      '<a href="javascript:void(0)" class="btn btn-default primary-btn light"><i class="fa fa-arrow-right"></i></a>',
    ],
    autoplay: false,
    center: true,
    slideSpeed: 300,
    paginationSpeed: 400,
    margin: 20,
    dots: false,
    loop: true,
    responsive: {
      0: {
        items: 1,
        margin: 20,
      },
      460: {
        items: 2,
      },
      991: {
        items: 3,
      },
    },
  };

  constructor() {}

  ngOnInit() {}
}
