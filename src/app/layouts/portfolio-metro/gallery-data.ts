export const data: any[] = [
  {
    id: 0,
    url: "https://picsum.photos/200/300",
    tags: ["portrait"],
  },
  {
    id: 1,
    url: "https://picsum.photos/300/300",
    tags: ["portrait"],
  },
  {
    id: 2,
    url: "https://picsum.photos/325/300",
    tags: ["evenementiel"],
  },
  {
    id: 3,
    url: "https://picsum.photos/350/300",
    tags: ["evenementiel"],
  },
  {
    id: 4,
    url: "https://picsum.photos/200/350",
    tags: ["portrait"],
  },
  {
    id: 5,
    url: "https://picsum.photos/200/325",
    tags: ["portrait"],
  },
  {
    id: 6,
    url: "https://picsum.photos/400/200",
    tags: ["animaux"],
  },
  {
    id: 7,
    url: "https://picsum.photos/200/300",
    tags: ["animaux"],
  },
  {
    id: 8,
    url: "https://picsum.photos/200/300",
    tags: ["animaux"],
  },
];
