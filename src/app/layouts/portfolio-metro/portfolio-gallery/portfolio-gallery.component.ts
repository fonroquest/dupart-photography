import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-portfolio-gallery",
  templateUrl: "./portfolio-gallery.component.html",
  styleUrls: ["./portfolio-gallery.component.scss"],
})
export class PortfolioGalleryComponent implements OnInit {
  public customizer: any = "all";

  mots = [];
  mots1 = ["animaux"];
  mots2 = ["portrait"];
  mots3 = ["evenementiel"];

  tags = ["animaux", "portrait", "evenementiel"];

  constructor() {}

  openGallery(val) {
    this.customizer = val;
  }

  ngOnInit() {}
}
