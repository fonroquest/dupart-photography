import { Component, OnInit, Input } from "@angular/core";
import {
  ButtonsConfig,
  ButtonsStrategy,
  AdvancedLayout,
  Image,
  KS_DEFAULT_BTN_CLOSE,
  KS_DEFAULT_BTN_DELETE,
  KS_DEFAULT_BTN_DOWNLOAD,
  KS_DEFAULT_BTN_EXTURL,
  KS_DEFAULT_BTN_FULL_SCREEN,
  PlainGalleryConfig,
  PlainGalleryStrategy,
} from "@ks89/angular-modal-gallery";

import { GalleryService } from "../../../services/gallery.service";

@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.scss"],
})
export class CategoryComponent implements OnInit {
  @Input() tags: string[];
  images: Image[];
  @Input() isSubMenuShown = false;

  submenu = ["famille", "corporate"];
  submenu1 = ["mariage", "bapteme"];
  submenu2 = ["exterieur", "mariage", "complicite"];

  buttonsConfigCustom: ButtonsConfig = {
    visible: true,
    strategy: ButtonsStrategy.CUSTOM,
    buttons: [
      KS_DEFAULT_BTN_FULL_SCREEN,
      KS_DEFAULT_BTN_DELETE,
      KS_DEFAULT_BTN_EXTURL,
      KS_DEFAULT_BTN_DOWNLOAD,
      KS_DEFAULT_BTN_CLOSE,
    ],
  };

  customPlainGalleryRowDescConfig: PlainGalleryConfig = {
    strategy: PlainGalleryStrategy.CUSTOM,
    layout: new AdvancedLayout(-1, true),
  };
  constructor(private galleryService: GalleryService) {}

  ngOnInit() {
    this.images = this.galleryService.getImagesViaTags(this.tags);
    console.log(this.tags);
  }

  openImageModalRowDescription(image: Image) {
    const index: number = this.getCurrentIndexCustomLayout(image, this.images);
    this.customPlainGalleryRowDescConfig = Object.assign(
      {},
      this.customPlainGalleryRowDescConfig,
      { layout: new AdvancedLayout(index, true) }
    );
  }

  private getCurrentIndexCustomLayout(image: Image, images: Image[]): number {
    return image ? images.indexOf(image) : -1;
  }
}
