import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.scss"],
})
export class ContactComponent implements OnInit {
  @Input() currentName = "";
  @Input() currentPhone = "";
  @Input() currentEmail = "";
  @Input() currentSubject = "";
  @Input() currentMessage = "";
  name = "";
  phone = "";
  email = "";
  subject = "";
  message = "";

  constructor() {}

  ngOnInit() {
    this.name = this.currentName;
    this.phone = this.currentPhone;
    this.email = this.currentEmail;
    this.subject = this.currentSubject;
    this.message = this.currentMessage;
  }

  submitForm() {
    console.log(this.name);
  }
}
