import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LayoutsRoutingModule } from "./layouts-routing.module";
import { CarouselModule } from "ngx-owl-carousel-o";
import {
  SwiperModule,
  SWIPER_CONFIG,
  SwiperConfigInterface,
} from "ngx-swiper-wrapper";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { SharedModule } from "../shared/shared.module";
import { CountToModule } from "angular-count-to";
import { GalleryModule } from "@ks89/angular-modal-gallery";
import { AngularTiltModule } from "angular-tilt";
import { ScrollToModule } from "ng2-scroll-to-el";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Ng5SliderModule } from "ng5-slider";
import { NgxMasonryModule } from "ngx-masonry";

// Portfolio Layout
import { PortfolioMetroComponent } from "./portfolio-metro/portfolio-metro.component";
import { PortfolioHeaderComponent } from "./portfolio-metro/header/portfolio-header.component";
import { PortfolioBreadcrumbComponent } from "./portfolio-metro/portfolio-breadcrumb/portfolio-breadcrumb.component";
import { PortfolioGalleryComponent } from "./portfolio-metro/portfolio-gallery/portfolio-gallery.component";
import { PortfolioFooterComponent } from "./portfolio-metro/portfolio-footer/portfolio-footer.component";
import { PortfolioCopyrightComponent } from "./portfolio-metro/portfolio-copyright/portfolio-copyright.component";
import { GalleryComponent } from "./portfolio-metro/portfolio-gallery/gallery/gallery.component";
import { CategoryComponent } from "./portfolio-metro/portfolio-gallery/category/category.component";
import { InstagramIntegrationComponent } from "./portfolio-metro/portfolio-instagram-integration/instagram-integration.component";
import { AboutComponent } from "./about/about.component";
import { ContactComponent } from "./portfolio-metro/portfolio-contact/contact.component";

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {};

@NgModule({
  declarations: [
    PortfolioMetroComponent,
    PortfolioGalleryComponent,
    PortfolioHeaderComponent,
    PortfolioBreadcrumbComponent,
    PortfolioFooterComponent,
    PortfolioCopyrightComponent,
    CategoryComponent,
    GalleryComponent,
    InstagramIntegrationComponent,
    AboutComponent,
    ContactComponent,
  ],

  imports: [
    CommonModule,
    LayoutsRoutingModule,
    SwiperModule,
    CarouselModule,
    NgbModule,
    GalleryModule.forRoot(),
    SharedModule,
    CountToModule,
    AngularTiltModule,
    ScrollToModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    Ng5SliderModule,
    NgxMasonryModule,
  ],

  exports: [],

  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG,
    },
  ],
})
export class LayoutsModule {}
